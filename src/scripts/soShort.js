import { soShort } from './components/Game'

function DOMIsReady() { if (document.getElementById('so-short')) soShort.gameInit() }

document.addEventListener('DOMContentLoaded', DOMIsReady);
