// Libraries
import { TweenMax, TimelineMax } from "gsap/TweenMax";
import Draggable from "gsap/Draggable";
import ThrowPropsPlugin from '../../common_libraries/ThrowPropsPlugin'
import SplitText from '../../common_libraries/SplitText'
import 'sharer.js'

// Custom modules
import {
    helperRandomInteger,
    helperFindIndex,
    helperShuffleArr,
    helperChunkArray
} from '../partials/helpers'
import { socNetShareBefore, socNetShareAfter } from '../partials/socNetShare'

class Game {
    constructor(id, gameLevelPhaseDataArr, gameResultsDataArr, gameTimer) {
        // ID
        this.id = id
        // Data
        this.gameLevelPhaseDataArr = gameLevelPhaseDataArr
        this.gameResultsDataArr = gameResultsDataArr
        this.gameUniqLettersCollection
        // Sections
        this.gameMotherContainer
        this.gameIntroBox
        this.gameLoaderBox
        this.gameGameplayBox
        this.gameResultBox
        // Values
        this.gameTimer = gameTimer
        this.gameTimerArch
        this.gameTimerCounter
        this.gameTimerCounterColor = 'rgba(255, 255, 255, 0.9)'
        this.gameStep
        this.gameCorrectAnswers
        this.gameInTotalSteps
        this.gameDataQuestionsSequence
        this.gameUniqRandomLetter
        this.gameCurrentFakeShuffleAbbArr
        this.gameCurrentTrueEmptyCellsNum
        this.gameCurrentTrueAbbArr
        this.gameCurrentHint
        this.gameCurrentTrueAnswer
        this.coordsTrueEmptyCells = []
    }

    gameInit() {

        this.gameStep = 0
        this.gameCorrectAnswers = 0
        this.gameInTotalSteps = this.gameLevelPhaseDataArr.length
        this.gameTimerArch = this.gameTimer

        let locHref = location.href

        if (locHref.indexOf('?result=') != -1) {
            let resultPosNum = locHref.indexOf('?result=')
            let pureLocHref = locHref.slice(0, resultPosNum)
            location.href = pureLocHref
        }

        this.gameMotherContainer = document.querySelector(`.${this.id}__content`)
        this.gameIntroBox = this.gameMotherContainer.querySelector(`.${this.id}__intro-box`)
        this.gameLoaderBox = this.gameMotherContainer.querySelector(`.${this.id}__loader-box`)
        this.gameGameplayBox = this.gameMotherContainer.querySelector(`.${this.id}__gameplay-box`)
        this.gameResultsBox = this.gameMotherContainer.querySelector(`.${this.id}__results-box`)

        console.log('game init')

        this.gameIntroBoxDisplaying()
        this.gameIntroPhaseActive()
    }
    gameClear() {

        this.gameStep = 0
        this.gameCorrectAnswers = 0
        this.gameDataQuestionsSequence = []
        this.gameTimer = this.gameTimerArch
        this.gameTimerCounterColor = 'rgba(255, 255, 255, 0.9)'
        this.gameGameplayBox.querySelector('.timer-box SPAN').style.color = 'rgba(255, 255, 255, 0.9)'
        this.gameGameplayBox.querySelector('.timer-box SPAN').textContent = this.gameTimer
        this.gameGameplayBox.querySelector('.controls-box BUTTON').textContent = 'Пропустить'
    }

    gameIntroPhaseActive() {

        const descIntroSection = this.gameIntroBox.querySelector('.description-box')
        const rulesIntroSection = this.gameIntroBox.querySelector('.rules-box')
        const playButton = this.gameIntroBox.querySelector('.description-box .controls-box BUTTON')
        const letsGoButton = this.gameIntroBox.querySelector('.rules-box .controls-box BUTTON')

        socNetShareBefore()

        this.gameSetUniqLettersCollection()

        this.gameQuestionCounterController()


        let splitTextDescTwo = new SplitText('.rules-box P',
            {
                type: 'words',
            }
        )

        let tl_RulesIntroSection_in = new TimelineMax()
        let tl_RulesIntroSection_out = new TimelineMax()

        playButton.onclick = () => {

            tl_RulesIntroSection_in
                .from('.rules-box P', 0.45, {autoAlpha: 0, x: 50, ease: Back.easeOut.config(1.7)})
                .staggerFrom(splitTextDescTwo.words, 0.4, {autoAlpha: 0, x: 40, y: 15, rotation: 20, ease: Elastic.easeIn}, 0.125, '-=0.35')
                // .from('.rules-box P SPAN', 0.65, {autoAlpha: 0, x: 40, ease: SteppedEase.config(12)}, '-=0.1')
                .from(letsGoButton, 0.65, {autoAlpha: 0, y: 20}, '-=0.15');

            descIntroSection.classList.add('game-actions__displaying-off')
            rulesIntroSection.classList.add('game-actions__displaying-on')
        }

        letsGoButton.onclick = () => {

            tl_RulesIntroSection_out
                .to('.rules-box P', 0.55, {
                    autoAlpha: 0,
                    x: 50,
                    ease: Back.easeOut.config(1.7)
                })
                .to(letsGoButton, 0.45, {
                    autoAlpha: 0
                }, '-=0.25')
                .to('.so-short__intro-box DIV H2', 0.25, {
                    autoAlpha: 0,
                    y: -20
                }, '-=0.35')
                .to('.so-short__intro-box .soc-net-share-box .soc-net-share-box__list', 0.25, {
                    autoAlpha: 0,
                    y: 20
                }, '-=0.25')

            setTimeout(() => {
                this.gameIntroBoxDisplaying()
                this.gameLoaderBoxDisplaying()
                this.gameQuestionGenerator()
                setTimeout(() => {
                    this.gameLoaderBoxDisplaying()
                    this.gameGameplayBoxDisplaying()
                    this.gameGameplayPhaseActive()
                }, 1750)
            }, 900)
        }
    }
    gameGameplayPhaseActive() {

        const skipButton = this.gameGameplayBox.querySelector('.controls-box BUTTON')

        const trueAnswerBlock = this.gameGameplayBox.querySelector('.true-answer-box')
        const trueAnswerBlockSpan = this.gameGameplayBox.querySelector('.true-answer-box SPAN')
        const trueAnswerBlockP = this.gameGameplayBox.querySelector('.true-answer-box P')

        this.gameLevelTimerInit()

        this.gameSetCurrentStep()

        this.gameSetCurrentHint()

        this.gameGenerateCells()

        this.gameDragnDropInit()

        skipButton.onclick = () => {

            this.gameLevelTimerPausing()

            TweenMax.to('.dragndrop-box__draggable-list', 0.15, {opacity: 0})
            TweenMax.to('.dragndrop-box__acceptingdrag-list', 0.15, {opacity: 0})

            this.gameSetCurrentTrueAnswer(trueAnswerBlock, trueAnswerBlockSpan, trueAnswerBlockP)

            skipButton.setAttribute('disabled', true)
            skipButton.style.backgroundColor = 'rgba(48, 47, 105, 1)'
            skipButton.style.opacity = '0.6'
            skipButton.style.cursor = 'not-allowed'

            setTimeout(() => {
                trueAnswerBlock.classList.remove('game-actions__displaying-on')
                this.gameGameplayBoxDisplaying()
                this.gameLoaderBoxDisplaying()

                TweenMax.set('.dragndrop-box__draggable-list', {opacity: 1})
                TweenMax.set('.dragndrop-box__acceptingdrag-list', {opacity: 1})

                this.gameStep >= 7 ?
                skipButton.textContent = 'Результаты' :
                skipButton.textContent = 'Пропустить'

                skipButton.removeAttribute('disabled')
                skipButton.style.backgroundColor = ''
                skipButton.style.opacity = ''
                skipButton.style.cursor = ''

                setTimeout(() => {
                    if  (this.gameStep === 8) {
                        this.gameLoaderBoxDisplaying()
                        this.gameResultsPhaseActive()
                        this.gameResultsBoxDisplaying()
                    } else {
                        this.gameQuestionGenerator()
                        this.gameSetCurrentStep()
                        this.gameSetCurrentHint()
                        this.gameLoaderBoxDisplaying()
                        this.gameGameplayBoxDisplaying()
                        this.gameGenerateCells()
                        this.gameDragnDropInit()
                        this.gameLevelTimerPlaying()
                    }
                }, 1250)
            }, 3250)

        }
    }
    gameResultsPhaseActive() {

        this.gameLevelTimerPausing()

        socNetShareAfter(this.gameCorrectAnswers, this.gameInTotalSteps)

        const correctAnswers = this.gameResultsBox.querySelector('.results-values__correct-answers')
        const inTotalQuestions = this.gameResultsBox.querySelector('.results-values__in-total-questions')
        const infoP = this.gameResultsBox.querySelector('.rectangle-box P')
        const replayGameButton = this.gameResultsBox.querySelector('.controls-box BUTTON')

        correctAnswers.textContent = this.gameCorrectAnswers
        inTotalQuestions.textContent = this.gameInTotalSteps

        if (this.gameCorrectAnswers <= 1) {
            infoP.textContent = this.gameResultsDataArr[0]
        } else if (this.gameCorrectAnswers > 1 && this.gameCorrectAnswers < 5) {
            infoP.textContent = this.gameResultsDataArr[1]
        } else if (this.gameCorrectAnswers > 4 && this.gameCorrectAnswers < 7) {
            infoP.textContent = this.gameResultsDataArr[2]
        } else if (this.gameCorrectAnswers >= 7) {
            infoP.textContent = this.gameResultsDataArr[3]
        }

        TweenMax.from(replayGameButton, 1, {autoAlpha: 0, ease: Power1.easeOut, delay: 1.75})

        replayGameButton.onclick = () => {

            this.gameClear()
            this.gameResultsBoxDisplaying()
            this.gameLoaderBoxDisplaying()
            this.gameQuestionCounterController()
            this.gameQuestionGenerator()
            setTimeout(() => {
                this.gameLoaderBoxDisplaying()
                this.gameGameplayBoxDisplaying()
                this.gameGameplayPhaseActive()
            }, 1750)
        }
    }

    gameIntroBoxDisplaying() {
        this.gameIntroBox.classList.toggle('game-actions__displaying-on--flex')
    }
    gameLoaderBoxDisplaying() {
        this.gameLoaderBox.classList.toggle('game-actions__displaying-on--flex')
        const lines = this.gameLoaderBox.querySelectorAll('.line')
        TweenMax.from(this.gameLoaderBox, 0.9, {autoAlpha: 0.75})
        TweenMax.from('.spinner-box', 0.35, {autoAlpha: 0, y: 5})
        TweenMax.staggerFrom(lines, 0.45, {y: -25, ease: SteppedEase.config(1)}, 0.25)
    }
    gameGameplayBoxDisplaying() {
        this.gameGameplayBox.classList.toggle('game-actions__displaying-on--flex')
        TweenMax.from(this.gameGameplayBox, 1, {autoAlpha: 0.7})
    }
    gameResultsBoxDisplaying() {
        this.gameResultsBox.classList.toggle('game-actions__displaying-on--flex')
        TweenMax.from(this.gameResultsBox, 1, {autoAlpha: 0.7})
    }

    gameSetUniqLettersCollection() {

        let lettersCollection = []
        for (let i = 0; i <= this.gameLevelPhaseDataArr.length - 1; i++) {
            lettersCollection.push(...this.gameLevelPhaseDataArr[i].abbArr)
        }
        this.gameUniqLettersCollection = [...new Set(lettersCollection)]
    }
    gameQuestionCounterController() {

        let randomDataObjNum;
        let randomDataObjArr = []

        while (randomDataObjArr.length < this.gameLevelPhaseDataArr.length) {

            randomDataObjNum = helperRandomInteger(1, this.gameLevelPhaseDataArr.length) - 1

            if (helperFindIndex(randomDataObjArr, randomDataObjNum) === -1) {
                randomDataObjArr.push(randomDataObjNum)
            }
        }

        this.gameDataQuestionsSequence = randomDataObjArr
    }
    gameQuestionGenerator() {

        let currentQuestion = this.gameDataQuestionsSequence.shift()
        let randomLetterFromGULC;
        let currentFakeCopiedAbbArr = []
        let currentFakeShuffleAbbArr;
        let currentTrueEmptyCellsNum;

        this.gameCurrentTrueAnswer = this.gameLevelPhaseDataArr[currentQuestion].abbFullAnswer

        this.gameCurrentTrueAbbArr = this.gameLevelPhaseDataArr[currentQuestion].abbArr

        const generateFakeAbbArr = () => {

            randomLetterFromGULC = this.gameUniqLettersCollection[helperRandomInteger(1, this.gameUniqLettersCollection.length) - 1]

            if (this.gameGenerateUniqRandomLetter(
                this.gameCurrentTrueAbbArr,
                randomLetterFromGULC
            )) {
                this.gameUniqRandomLetter = randomLetterFromGULC
                currentFakeCopiedAbbArr.push([...this.gameCurrentTrueAbbArr, randomLetterFromGULC])
                currentFakeShuffleAbbArr = helperShuffleArr(currentFakeCopiedAbbArr[0])
                this.gameCurrentFakeShuffleAbbArr = currentFakeShuffleAbbArr
            } else {
                generateFakeAbbArr()
            }
        }

        generateFakeAbbArr()

        const generateTrueEmptyCellsNum = () => {

            currentTrueEmptyCellsNum = this.gameLevelPhaseDataArr[currentQuestion].abbArr.length
            this.gameCurrentTrueEmptyCellsNum = currentTrueEmptyCellsNum
        }

        generateTrueEmptyCellsNum()

        const setCurrentHint = () => {
            this.gameCurrentHint = this.gameLevelPhaseDataArr[currentQuestion].hint
        }

        setCurrentHint()

        this.gameStepsCounter()
    }
    gameGenerateUniqRandomLetter(lettersArr, randomLetter) {
        return lettersArr.every(letter => letter.toString() !== randomLetter.toString())
    }
    gameStepsCounter() {
        this.gameStep++
    }
    gameSetCorrectAnswers() {
        this.gameCorrectAnswers++
    }
    gameLevelTimerInit() {

        const timer = this.gameGameplayBox.querySelector('.timer-box SPAN')

        let timerCounter = setInterval(() => {

            this.gameTimer--

            TweenMax.fromTo(timer, 0.85, {opacity: 0.5}, {opacity: 1})

            timer.textContent = this.gameTimer

            if (this.gameTimer < 60) {
                this.gameTimerCounterColor = 'rgba(250, 157, 10, 0.9)'
                timer.style.color = this.gameTimerCounterColor
            }

            if (this.gameTimer < 20) {
                this.gameTimerCounterColor = 'rgba(254, 56, 23, 0.9)'
                timer.style.color = this.gameTimerCounterColor
            }

            if (this.gameTimer <= 0) {
                clearInterval(timerCounter)
                setTimeout(() => {
                    this.gameGameplayBoxDisplaying()
                    this.gameLoaderBoxDisplaying()
                    setTimeout(() => {
                        this.gameLoaderBoxDisplaying()
                        this.gameResultsBoxDisplaying()
                        this.gameResultsPhaseActive()
                    }, 1000)
                }, 1000)
            }
        }, 1000)

        this.gameTimerCounter = timerCounter
    }
    gameLevelTimerPlaying() {
        const timerBox = this.gameGameplayBox.querySelector('.timer-box')
        const timer = this.gameGameplayBox.querySelector('.timer-box SPAN')
        TweenMax.from(timerBox, 0.45, {autoAlpha: 0, ease: Power3.easeOut})
        TweenMax.fromTo(timer, 1.25, {color: 'rgba(44, 255, 79, 0.9)'}, {color: this.gameTimerCounterColor})
        this.gameLevelTimerInit()
    }
    gameLevelTimerPausing() {
        const timer = this.gameGameplayBox.querySelector('.timer-box SPAN')
        clearInterval(this.gameTimerCounter)
        timer.style.color = 'rgba(126, 124, 253, 0.9)'
    }
    gameSetCurrentStep() {
        const currentStep = this.gameGameplayBox.querySelector('.steps-box__current')
        currentStep.textContent = this.gameStep
        TweenMax.from(currentStep, 0.75, {autoAlpha: 0, y: -85, ease: Power3.easeOut})
    }
    gameSetCurrentHint() {
        const hintP = this.gameGameplayBox.querySelector('.hint-box P')
        hintP.innerHTML = this.gameCurrentHint
        TweenMax.from(hintP, 2, {opacity: 0, x: 20})
    }
    gameSetCurrentTrueAnswer(tab, tabSpan, tabP) {

        tabP.textContent = this.gameCurrentTrueAnswer
        tab.classList.add('game-actions__displaying-on')

        let splitTextTabP = new SplitText(tabP,
            {
                type: 'words, chars',
            }
        )
        TweenMax.staggerFrom(splitTextTabP.chars, 0.1, {autoAlpha: 0, x: 10, y: 1, rotation: 22}, 0.025)
        TweenMax.from(tab, 0.65, {autoAlpha: 0, y: -15})
        TweenMax.from(tabSpan, 0.2, {opacity: 0, delay: 0.1})
    }
    gameClearCells(list1, list2) {

        while (list1.firstChild) {
            list1.removeChild(list1.firstChild);
        }
        while (list2.firstChild) {
            list2.removeChild(list2.firstChild);
        }
    }
    gameGenerateCells() {

        const dragnDropBox = this.gameGameplayBox.querySelector('.dragndrop-box')
        const dragnDropDraggableList = this.gameGameplayBox.querySelector('.dragndrop-box__draggable-list')
        const dragnDropAcceptingDragList = this.gameGameplayBox.querySelector('.dragndrop-box__acceptingdrag-list')

        this.gameClearCells(dragnDropDraggableList, dragnDropAcceptingDragList)

        const generateFakeFilledCells = arr => {

            for (let i = 0; i < arr.length; i++) {
                let li = document.createElement('li')
                li.className = 'dragndrop-box__draggable-item'
                li.setAttribute('data-letter', arr[i])
                li.textContent = arr[i]
                dragnDropDraggableList.insertBefore(
                    li,
                    dragnDropDraggableList.childNodes[dragnDropDraggableList.childNodes[i]]
                )
            }

            let tl_DraggableItems_in = new TimelineMax()

            tl_DraggableItems_in
                .staggerFrom('.dragndrop-box__draggable-item', 1.2, {
                    autoAlpha: 0,
                    y: -90,
                    rotation: 25,
                    ease: Bounce.easeOut
                }, 0.35)
        }

        generateFakeFilledCells(this.gameCurrentFakeShuffleAbbArr)

        const generateTrueEmptyCells = num => {

            for (let i = 0; i < num; i++) {
                let li = document.createElement('li')
                li.className = 'dragndrop-box__acceptingdrag-item'
                li.setAttribute('data-filled', false)
                dragnDropAcceptingDragList.insertBefore(li, dragnDropAcceptingDragList.childNodes[0])
            }

            TweenMax.from('.dragndrop-box__acceptingdrag-list', 1, {autoAlpha: 0, y: 30, ease: Circ.easeOut})
        }

        generateTrueEmptyCells(this.gameCurrentTrueEmptyCellsNum)
    }
    gameDragnDropInit() {

        const dragnDropBox = this.gameGameplayBox.querySelector('.dragndrop-box')
        const dragnDropItems = Array.from(this.gameGameplayBox.querySelectorAll('.dragndrop-box__draggable-item'))
        const targetItems = Array.from(this.gameGameplayBox.querySelectorAll('.dragndrop-box__acceptingdrag-item'))

        const trueAnswerBlock = this.gameGameplayBox.querySelector('.true-answer-box-2')
        const trueAnswerBlockSpan = this.gameGameplayBox.querySelector('.true-answer-box-2 SPAN')
        const trueAnswerBlockP = this.gameGameplayBox.querySelector('.true-answer-box-2 P')

        const skipButton = this.gameGameplayBox.querySelector('.controls-box BUTTON')

        const thresholdValue = '25%'

        let correctLetters = 0

        let boundsTop = this.gameGameplayBox.querySelector('.dragndrop-box__draggable-list').getBoundingClientRect().top
        let boundsLeft = this.gameGameplayBox.querySelector('.dragndrop-box__draggable-list').getBoundingClientRect().left


        let dragnDropElementTop = Math.floor(dragnDropItems[0].getBoundingClientRect().top) - boundsTop
        let dragnDropElementLeft;

        let targetElementTop = Math.floor(targetItems[0].getBoundingClientRect().top) - boundsTop
        let targetElementLeft;

        let offsetTop = targetElementTop - dragnDropElementTop - 130
        let offsetLeft;

        let self = this
        let currentTrueAbbArr = this.gameCurrentTrueAbbArr
        let currentTrueEmptyCellsNum = this.gameCurrentTrueEmptyCellsNum
        let uniqRandomLetter = this.gameUniqRandomLetter

        const setCorrectAnswers = this.gameSetCorrectAnswers

        dragnDropItems.forEach(dragnDropItem => {

            let dragnDropEntity = new Draggable.create(dragnDropItem, {
                bounds: dragnDropBox,
                edgeResistance: 0.75,
                type: "x, y",
                throwProps: true,
                throwResistance: 0.5,
                onDragEnd: function() {

                    targetItems.forEach((targetItem, index) => {

                        if (this.hitTest(targetItem, thresholdValue)) {

                            if (
                                this.target.attributes[1].value == currentTrueAbbArr[index]
                                &&
                                targetItem.getAttribute('data-filled') != 'true'
                            ) {

                                if ((window.innerWidth || document.documentElement.clientWidth) < 810) {

                                    if (this.target == dragnDropItems[0]) {
                                        TweenMax.to(this.target , 1, {
                                            x: Math.floor(targetItems[index].getBoundingClientRect().left - 18) - boundsLeft + 'px',
                                            y: offsetTop + 3 + 'px',
                                            backgroundColor: '#26d367',
                                            borderColor: '#20b859',
                                            boxShadow: '0px 0px 0px 3px rgba(255, 255, 255, 0.3)',
                                            ease: Elastic.easeOut,
                                            onStart: function() {
                                                targetItems[index].style.borderColor = '#20b859'
                                            }
                                        })
                                        this.disable()
                                        targetItem.setAttribute('data-filled', true)
                                        correctLetters++
                                    } else if (this.target == dragnDropItems[1]) {
                                        TweenMax.to(this.target , 1, {
                                            x: Math.floor(targetItems[index].getBoundingClientRect().left - 78) - boundsLeft + 'px',
                                            y: offsetTop + 3 + 'px',
                                            backgroundColor: '#26d367',
                                            borderColor: '#20b859',
                                            boxShadow: '0px 0px 0px 3px rgba(255, 255, 255, 0.3)',
                                            ease: Elastic.easeOut,
                                            onStart: function() {
                                                targetItems[index].style.borderColor = '#20b859'
                                            }
                                        })
                                        this.disable()
                                        targetItem.setAttribute('data-filled', true)
                                        correctLetters++
                                    } else if (this.target == dragnDropItems[2]) {
                                        TweenMax.to(this.target , 1, {
                                            x: Math.floor(targetItems[index].getBoundingClientRect().left - 139) - boundsLeft + 'px',
                                            y: offsetTop + 3 + 'px',
                                            backgroundColor: '#26d367',
                                            borderColor: '#20b859',
                                            boxShadow: '0px 0px 0px 3px rgba(255, 255, 255, 0.3)',
                                            ease: Elastic.easeOut,
                                            onStart: function() {
                                                targetItems[index].style.borderColor = '#20b859'
                                            }
                                        })
                                        this.disable()
                                        targetItem.setAttribute('data-filled', true)
                                        correctLetters++
                                    } else if (this.target == dragnDropItems[3]) {
                                        TweenMax.to(this.target , 1, {
                                            x: Math.floor(targetItems[index].getBoundingClientRect().left - 199) - boundsLeft + 'px',
                                            y: offsetTop + 3 + 'px',
                                            backgroundColor: '#26d367',
                                            borderColor: '#20b859',
                                            boxShadow: '0px 0px 0px 3px rgba(255, 255, 255, 0.3)',
                                            ease: Elastic.easeOut,
                                            onStart: function() {
                                                targetItems[index].style.borderColor = '#20b859'
                                            }
                                        })
                                        this.disable()
                                        targetItem.setAttribute('data-filled', true)
                                        correctLetters++
                                    } else if (this.target == dragnDropItems[4]) {
                                        TweenMax.to(this.target , 1, {
                                            x: Math.floor(targetItems[index].getBoundingClientRect().left - 258) - boundsLeft + 'px',
                                            y: offsetTop + 2 + 'px',
                                            backgroundColor: '#26d367',
                                            borderColor: '#20b859',
                                            boxShadow: '0px 0px 0px 3px rgba(255, 255, 255, 0.3)',
                                            ease: Elastic.easeOut,
                                            onStart: function() {
                                                targetItems[index].style.borderColor = '#20b859'
                                            }
                                        })
                                        this.disable()
                                        targetItem.setAttribute('data-filled', true)
                                        correctLetters++
                                    }
                                } else {
                                if (this.target == dragnDropItems[0]) {
                                    TweenMax.to(this.target , 1, {
                                        x: Math.floor(targetItems[index].getBoundingClientRect().left - 17) - boundsLeft + 'px',
                                        y: offsetTop + 'px',
                                        backgroundColor: '#26d367',
                                        borderColor: '#20b859',
                                        boxShadow: '0px 0px 0px 3px rgba(255, 255, 255, 0.3)',
                                        ease: Elastic.easeOut,
                                        onStart: function() {
                                            targetItems[index].style.borderColor = '#20b859'
                                        }
                                    })
                                    this.disable()
                                    targetItem.setAttribute('data-filled', true)
                                    correctLetters++
                                } else if (this.target == dragnDropItems[1]) {
                                    TweenMax.to(this.target , 1, {
                                        x: Math.floor(targetItems[index].getBoundingClientRect().left - 86) - boundsLeft + 'px',
                                        y: offsetTop + 'px',
                                        backgroundColor: '#26d367',
                                        borderColor: '#20b859',
                                        boxShadow: '0px 0px 0px 3px rgba(255, 255, 255, 0.3)',
                                        ease: Elastic.easeOut,
                                        onStart: function() {
                                            targetItems[index].style.borderColor = '#20b859'
                                        }
                                    })
                                    this.disable()
                                    targetItem.setAttribute('data-filled', true)
                                    correctLetters++
                                } else if (this.target == dragnDropItems[2]) {
                                    TweenMax.to(this.target , 1, {
                                        x: Math.floor(targetItems[index].getBoundingClientRect().left - 154) - boundsLeft + 'px',
                                        y: offsetTop + 'px',
                                        backgroundColor: '#26d367',
                                        borderColor: '#20b859',
                                        boxShadow: '0px 0px 0px 3px rgba(255, 255, 255, 0.3)',
                                        ease: Elastic.easeOut,
                                        onStart: function() {
                                            targetItems[index].style.borderColor = '#20b859'
                                        }
                                    })
                                    this.disable()
                                    targetItem.setAttribute('data-filled', true)
                                    correctLetters++
                                } else if (this.target == dragnDropItems[3]) {
                                    TweenMax.to(this.target , 1, {
                                        x: Math.floor(targetItems[index].getBoundingClientRect().left - 222) - boundsLeft + 'px',
                                        y: offsetTop + 'px',
                                        backgroundColor: '#26d367',
                                        borderColor: '#20b859',
                                        boxShadow: '0px 0px 0px 3px rgba(255, 255, 255, 0.3)',
                                        ease: Elastic.easeOut,
                                        onStart: function() {
                                            targetItems[index].style.borderColor = '#20b859'
                                        }
                                    })
                                    this.disable()
                                    targetItem.setAttribute('data-filled', true)
                                    correctLetters++
                                } else if (this.target == dragnDropItems[4]) {
                                    TweenMax.to(this.target , 1, {
                                        x: Math.floor(targetItems[index].getBoundingClientRect().left - 289) - boundsLeft + 'px',
                                        y: offsetTop + 'px',
                                        backgroundColor: '#26d367',
                                        borderColor: '#20b859',
                                        boxShadow: '0px 0px 0px 3px rgba(255, 255, 255, 0.3)',
                                        ease: Elastic.easeOut,
                                        onStart: function() {
                                            targetItems[index].style.borderColor = '#20b859'
                                        }
                                    })
                                    this.disable()
                                    targetItem.setAttribute('data-filled', true)
                                    correctLetters++
                                }
                            }
                            } else {
                                TweenMax.fromTo(this.target, 1, {
                                    backgroundColor: 'rgba(254, 56, 23, 1)',
                                }, {
                                    backgroundColor: '#f57a4a', x: 0, y: 0, ease: Bounce.easeOut}
                                )
                            }

                            if (correctLetters == currentTrueEmptyCellsNum) {
                                self.gameCorrectAnswers++
                                TweenMax.from(dragnDropBox, 2.2, {backgroundColor: 'rgba(255, 255, 255, 0.8)'})
                                dragnDropItems.forEach(dragnDropItem => {
                                    if (dragnDropItem.dataset.letter == uniqRandomLetter) {

                                        self.gameLevelTimerPausing()

                                        TweenMax.to(dragnDropItem, 1, {y: -300, ease: Bounce.easeIn})

                                        skipButton.setAttribute('disabled', true)
                                        skipButton.style.backgroundColor = 'rgba(48, 47, 105, 1)'
                                        skipButton.style.opacity = '0.6'
                                        skipButton.style.cursor = 'not-allowed'

                                        setTimeout(() => {
                                            TweenMax.to('.dragndrop-box__draggable-list', 0.15, {opacity: 0})
                                            TweenMax.to('.dragndrop-box__acceptingdrag-list', 0.15, {opacity: 0})
                                            self.gameSetCurrentTrueAnswer(trueAnswerBlock, trueAnswerBlockSpan, trueAnswerBlockP)}
                                            , 1000)

                                        setTimeout(() => {
                                            trueAnswerBlock.classList.remove('game-actions__displaying-on')
                                            self.gameGameplayBoxDisplaying()
                                            self.gameLoaderBoxDisplaying()

                                            TweenMax.set('.dragndrop-box__draggable-list', {opacity: 1})
                                            TweenMax.set('.dragndrop-box__acceptingdrag-list', {opacity: 1})

                                            skipButton.removeAttribute('disabled')
                                            skipButton.style.backgroundColor = ''
                                            skipButton.style.opacity = ''
                                            skipButton.style.cursor = ''

                                            self.gameStep >= 7 ?
                                            self.gameGameplayBox.querySelector('.controls-box BUTTON').textContent = 'Результаты' :
                                            self.gameGameplayBox.querySelector('.controls-box BUTTON').textContent = 'Пропустить'

                                            setTimeout(() => {
                                                if (self.gameStep === 8) {

                                                    self.gameLoaderBoxDisplaying()
                                                    self.gameResultsPhaseActive()
                                                    self.gameResultsBoxDisplaying()
                                                } else {
                                                    self.gameQuestionGenerator()
                                                    self.gameSetCurrentStep()
                                                    self.gameSetCurrentHint()
                                                    self.gameLoaderBoxDisplaying()
                                                    self.gameGameplayBoxDisplaying()
                                                    self.gameGenerateCells()
                                                    self.gameDragnDropInit()
                                                    self.gameLevelTimerPlaying()
                                                }
                                            }, 1250)
                                        }, 3250)
                                    }
                                })

                            }
                        }
                    })

                }
            })
        })
    }
}

export const soShort = new Game(
    'so-short',
    [
        {
            abbArr: ['М', 'К', 'А', 'Д'],
            abbFullAnswer: 'МКАД — Московская кольцевая автодорога',
            hint: 'Граница Москвы и Московской области — в сознании многих москвичей.'
        },
        {
            abbArr: ['Т', 'Т', 'К'],
            abbFullAnswer: 'ТТК — Третье транспортное кольцо',
            hint: 'Не было первым, но считать начали с него.'
        },
        {
            abbArr: ['Ц', 'О', 'Д', 'Д'],
            abbFullAnswer: 'ЦОДД — Центр организации дорожного движения',
            hint: 'Не транспорт, не дорога, а орган управления тем и другим.'
        },
        {
            abbArr: ['М', 'Ц', 'К'],
            abbFullAnswer: 'МЦК — Московское центральное кольцо',
            hint: 'Похоже и на метро, и на электричку.'
        },
        {
            abbArr: ['Б', 'К', 'Л'],
            abbFullAnswer: 'БКЛ — Большая кольцевая линия, 11-я линия Московского метрополитена',
            hint: 'На схемах была и бирюзовой, и чёрной.'
        },
        {
            abbArr: ['Т', 'П', 'К'],
            abbFullAnswer: 'ТПК — Третий пересадочный контур',
            hint: 'Рабочее название БКЛ?'
        },
        {
            abbArr: ['С', 'В', 'Х'],
            abbFullAnswer: 'СВХ — Северо-Восточная хорда',
            hint: 'Одна из бессветофорных магистральных трасс, которая сейчас строится в Москве.'
        },
        {
            abbArr: ['М', 'Ц', 'Д'],
            abbFullAnswer: 'МЦД — Московские центральные диаметры',
            hint: 'В прессе этот проект называют “наземным метро”, хотя формально он к метрополитену не относится.'
        },
    ],
    [
        'Добро пожаловать в Москву! ;)',
        'Главное — ничего не путайте, когда планируете очередной маршрут!',
        'Очень хороший результат, учитывая, как быстро развивается транспортная система столицы!',
        'Транспортные аббревиатуры Москвы вы знаете отлично! Счастливого пути!'
    ],
    120
)
