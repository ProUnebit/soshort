let gameURL = location.href + '?result='
// let gameImagesPath = 'https://www.m24.ru/special/s/testGame_v4?result='

export const socNetShareBefore = () => {

    // let metaImage = document.createElement('meta')
    // metaImage.setAttribute('property', 'og:image')
    // metaImage.content = "https://www.m24.ru/special/img/koroche/fx-default.png"
    // document.getElementsByTagName('head')[0].appendChild(metaImage)

    // let metaImageTwitter = document.createElement('meta')
    // metaImageTwitter.setAttribute('property', 'twitter:image')
    // metaImageTwitter.content = "https://www.m24.ru/special/img/koroche/fx-default.png"
    // document.getElementsByTagName('head')[0].appendChild(metaImageTwitter)

    // VK
    document.querySelector('.soc-net-share-box__item--vk--before-game').addEventListener('click', function() {
        window.open(`http://vk.com/share.php?url=${encodeURIComponent(gameURL)}&title=${'Игра "Так короче!"'}`, '', 'left=0,top=0,width=600,height=300,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')
    })
    // FA
    document.querySelector('.soc-net-share-box__item--fa--before-game').setAttribute('data-url', gameURL)
    document.querySelector('.soc-net-share-box__item--fa--before-game').setAttribute('data-hashtag', 'таккороче')
    document.querySelector('.soc-net-share-box__item--fa--before-game').setAttribute('data-title', 'Игра "Так короче!"')
    // TW
    document.querySelector('.soc-net-share-box__item--tw--before-game').addEventListener('click', function() {
        window.open(`https://twitter.com/intent/tweet?url=${encodeURIComponent(gameURL)}&text=${'Игра "Так короче!"'}&hashtags=${'таккороче, игра, москва24'}`, '', 'left=0,top=0,width=600,height=300,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')
    })
    // TE
    document.querySelector('.soc-net-share-box__item--te--before-game').addEventListener('click', function() {
        window.open(`https://t.me/share/url?url=${encodeURIComponent(gameURL)}&text=${'Игра "Так короче!"'}`, '', 'left=0,top=0,width=600,height=300,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')
    })
}

export const socNetShareAfter = (correctAnswers, inTotalSteps) => {

    Array.from(document.getElementsByTagName('head')[0].querySelectorAll('META')).forEach(metaTag => {
        if (metaTag.getAttribute('property') == 'og:title') {
            metaTag.setAttribute('content', `Игра "Так короче!". Мой результат: ${correctAnswers} из ${inTotalSteps}`)
            return;
        }
    })

    let correctAnswersPlusOne = correctAnswers + 1

    // VK
    document.querySelector('.soc-net-share-box__item--vk--after-game').onclick = function() {
        window.open(`http://vk.com/share.php?url=${encodeURIComponent(gameURL)}${correctAnswersPlusOne}&title=${'Игра "Так короче!". Мой результат: ' + correctAnswers + ' из ' + inTotalSteps}`, '', 'left=0,top=0,width=600,height=300,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')
    }
    // FA
    document.querySelector('.soc-net-share-box__item--fa--after-game').setAttribute('data-url', gameURL + correctAnswersPlusOne)
    document.querySelector('.soc-net-share-box__item--fa--after-game').setAttribute('data-hashtag', 'таккороче')
    document.querySelector('.soc-net-share-box__item--fa--after-game').setAttribute('data-title', 'Игра "Так короче!". Мой результат: ' + correctAnswers + ' из ' + inTotalSteps)
    // TW
    document.querySelector('.soc-net-share-box__item--tw--after-game').onclick = function() {
        window.open(`https://twitter.com/intent/tweet?url=${encodeURIComponent(gameURL)}${correctAnswersPlusOne}&text=${'Игра "Так короче!". Мой результат: ' + correctAnswers + ' из ' + inTotalSteps}&hashtags=${'таккороче, москва24'}`, '', 'left=0,top=0,width=600,height=300,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')
    }
    // TE
    document.querySelector('.soc-net-share-box__item--te--after-game').onclick = function() {
        window.open(`https://t.me/share/url?url=${encodeURIComponent(gameURL)}${correctAnswersPlusOne}&text=${'Игра "Так короче!". Мой результат: ' + correctAnswers + ' из ' + inTotalSteps}`, '', 'left=0,top=0,width=600,height=300,menubar=no,toolbar=no,resizable=yes,scrollbars=yes')
    }
}
